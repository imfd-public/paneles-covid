# Paneles Covid

Este repositorio contiene la aplicación web disponible en [covid19.imfd.cl](https://covid19.imfd.cl).

## Datos
Para crear los paneles transformamos las planillas disponibles en el [repositorio de la Universidad Johns Hopkins](https://github.com/CSSEGISandData/COVID-19/) y en el [repositorio del Ministerio de Ciencia](https://github.com/MinCiencia/Datos-COVID19/) a una base de datos relacional. El proceso para crear dicha base de datos es completamente abierto, puedes ver todos los detalles y generar la base de datos tú mismo usando [este repositorio](https://gitlab.com/imfd-public/aggregated-covid-data).

## Paneles
A continuación se describen los paneles que se muestran, qué puede elegir el usuario en cada uno, y se presentan los links a los dashboards correspondientes hechos en [Metabase](https://metabase.com). El objetivo de disponibilizar los links es simplemente para documentar, **los usuarios pueden ver todos los paneles en [covid19.imfd.cl](https://covid19.imfd.cl) y no deberían acceder directamente a estos links**, ya que las opciones ahí son confusas.

### Situación general en Chile
El usuario podrá elegir la fecha de inicio para los gráficos temporales, y si quiere ver las comparaciones dentro del país (por ejemplo los rankings de regiones) en tasas o en totales.
 - [x] [A partir de una fecha, comparando en tasas](http://metabase.imfd.cl/public/dashboard/ac48de05-4146-4d88-81e4-465cf0717bc6)
 - [x] [A partir de una fecha, comparando en totales](http://metabase.imfd.cl/public/dashboard/1288bc85-d2cb-41f4-8abc-1dcd56ae8263)

### Situación general mundial
El usuario podrá elegir la fecha de inicio para los gráficos temporales, y si quiere ver las comparaciones de países en tasas o en totales.
 - [x] [A partir de una fecha, comparando en tasas](http://metabase.imfd.cl/public/dashboard/6f478cca-ed02-485f-a902-c50b14458261)
 - [x] [A partir de una fecha, comparando en totales](http://metabase.imfd.cl/public/dashboard/7ae75c7d-49cd-4383-b73c-613f275f122e)

### Comparativo Países

En este panel el usuario primero puede elegir el conjunto de países a comparar. Puede también elegir si ver tasas o totales, y si quiere comparar a partir de una fecha o a partir de un cierto hito. En caso de comparar a partir de un cierto hito, el hito se podrá medir a su vez en tasas o en totales. Los links a los paneles correspondientes son: 

 - [x] [Ver totales a partir de una fecha](http://metabase.imfd.cl/public/dashboard/17b70a0c-9805-4e71-a724-68e18f2000d2)
 - [x] [Ver tasas a partir de una fecha](http://metabase.imfd.cl/public/dashboard/8f0ab1d0-42b3-47db-a15b-f1796af170aa)
 - [x] [Ver totales a partir de `N` pacientes declarados `confirmados/fallecidos/recuperados` por `100k` habitantes](http://metabase.imfd.cl/public/dashboard/bf6ad9c4-e5a1-4fbb-b02e-2dbf1c1ed8d6)
 - [x] [Ver totales a partir de `N` pacientes declarados `confirmados/fallecidos/recuperados` en total](http://metabase.imfd.cl/public/dashboard/6df232fc-f6f6-45bc-ad48-37f208bbd402)
 - [x] [Ver tasas a partir de `N` pacientes declarados `confirmados/fallecidos/recuperados` por `100k` habitantes](http://metabase.imfd.cl/public/dashboard/4a64e49e-2c42-4ef1-a839-5cf0139e538a)
 - [x] [Ver tasas a partir de `N` pacientes declarados `confirmados/fallecidos/recuperados` en total](http://metabase.imfd.cl/public/dashboard/fe018705-4d1a-4afa-b9d7-fb3e7bb83fdf)

### Comparativo Regiones

En este panel las opciones son las mismas que en el comparativo de países, pero en vez de elegir países el usuario elegirá regiones. Los links a los paneles son.

 - [x] [Ver totales a partir de una fecha](http://metabase.imfd.cl/public/dashboard/91aed47a-8085-4d00-a88f-5f058f51c769)
 - [x] [Ver tasas a partir de una fecha](http://metabase.imfd.cl/public/dashboard/4372cd0d-7625-49a9-a3d7-12e1dc60631c)
 - [x] [Ver totales a partir de `N` pacientes declarados `confirmados/fallecidos` por `100k` habitantes](http://metabase.imfd.cl/public/dashboard/f5fe377f-6f5f-441d-a7ca-ba20a1a01f7e)
 - [x] [Ver totales a partir de `N` pacientes declarados `confirmados/fallecidos` en total](http://metabase.imfd.cl/public/dashboard/d29114ea-5857-48e9-bce7-8d5618d059bb)
 - [x] [Ver tasas a partir de `N` pacientes declarados `confirmados/fallecidos` por `100k` habitantes](http://metabase.imfd.cl/public/dashboard/230655b1-5489-4d80-9285-e690bc79f8db)
 - [x] [Ver tasas a partir de `N` pacientes declarados `confirmados/fallecidos` en total](http://metabase.imfd.cl/public/dashboard/688ae4ee-e0cd-4874-bab0-25656150dc22)

### Perfil de un país
El usuario podrá elegir el país que quiere ver y la fecha de inicio para los gráficos temporales.
 - [x] [A partir de una fecha](http://metabase.imfd.cl/public/dashboard/61f6f911-496f-4469-989e-721370a26315)

### Perfil de una región
El usuario podrá elegir la región que quiere ver y la fecha de inicio para los gráficos temporales.
 - [x] [A partir de una fecha](http://metabase.imfd.cl/public/dashboard/40d8708f-ff2f-4e1e-881b-b02fe233a1f0)
