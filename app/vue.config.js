module.exports = {
  chainWebpack: config => {
    config.module
      .rule('raw')
      .test(/\.md/)
      .use('raw-loader')
      .loader('raw-loader')
      .end()
  },

  pluginOptions: {
    moment: {
      locales: ['es-cl', 'en-us']
    }
  }
}
