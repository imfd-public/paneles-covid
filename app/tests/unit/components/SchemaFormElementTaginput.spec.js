import SchemaFormElementTaginput from '@/components/SchemaFormElementTaginput'
import { mount, createLocalVue } from '@vue/test-utils'
import buefyConfig from '@/buefy-config'

const mountTaginput = props => {
  const localVue = createLocalVue()
  buefyConfig(localVue)

  return mount(SchemaFormElementTaginput, {
    localVue,
    propsData: props
  })
}

const testSchema = {
  title: 'A title',
  type: 'array',
  examples: ['Argentina', 'Chile', 'Brasil', 'China'],
  default: ['Chile', 'Argentina']
}

describe('The Taginput element for JSON Schema forms', () => {
  it('Binds the default value', () => {
    const wrapper = mountTaginput({
      schema: testSchema
    })

    const tags = wrapper.findAll('.tag')
    const tagsHtml = tags.wrappers.map(w => w.html()).reduce((acc, val) => `${acc} ${val}`, '')

    expect(tagsHtml).toContain('Chile')
    expect(tagsHtml).toContain('Argentina')
    expect(tagsHtml).not.toContain('China')
    expect(tagsHtml).not.toContain('Brasil')
  })

  it('Binds the initial value', () => {
    const wrapper = mountTaginput({
      schema: testSchema,
      value: ['Brasil']
    })

    const tags = wrapper.findAll('.tag')
    const tagsHtml = tags.wrappers.map(w => w.html()).reduce((acc, val) => `${acc} ${val}`, '')

    expect(tagsHtml).toContain('Brasil')
    expect(tagsHtml).not.toContain('Chile')
    expect(tagsHtml).not.toContain('Argentina')
    expect(tagsHtml).not.toContain('China')
  })

  it('Makes suggestions', async () => {
    const wrapper = mountTaginput({
      schema: testSchema,
      value: []
    })

    const input = wrapper.find('input')
    input.setValue('Chi')

    await wrapper.vm.$nextTick()

    const suggestionsHtml = wrapper
      .findAll('.dropdown-item')
      .wrappers.map(w => w.html())
      .reduce((acc, val) => `${acc} ${val}`, '')

    expect(suggestionsHtml).toContain('Chile')
    expect(suggestionsHtml).toContain('China')
  })

  it('Does not suggest elements that have already been selected', async () => {
    const wrapper = mountTaginput({
      schema: testSchema,
      value: ['China']
    })

    const input = wrapper.find('input')
    input.setValue('Chi')

    await wrapper.vm.$nextTick()

    const suggestionsHtml = wrapper
      .findAll('.dropdown-item')
      .wrappers.map(w => w.html())
      .reduce((acc, val) => `${acc} ${val}`, '')

    expect(suggestionsHtml).toContain('Chile')
    expect(suggestionsHtml).not.toContain('China')
  })

  it('Suggests every country that has not been selected when some country is accepted as a suggestion', async () => {
    const wrapper = mountTaginput({
      schema: testSchema,
      value: []
    })

    const input = wrapper.find('input')
    input.setValue('Chi')

    await wrapper.vm.$nextTick()

    const autocomplete = wrapper.find({ name: 'BAutocomplete' })
    autocomplete.vm.$emit('select', 'Chile')

    await wrapper.vm.$nextTick()

    const suggestionsHtml = wrapper
      .findAll('.dropdown-item')
      .wrappers.map(w => w.html())
      .reduce((acc, val) => `${acc} ${val}`, '')

    expect(suggestionsHtml).not.toContain('Chile')
    expect(suggestionsHtml).toContain('Argentina')
    expect(suggestionsHtml).toContain('Brasil')
    expect(suggestionsHtml).toContain('China')
  })
})
