import Vue from 'vue'
import App from './App.vue'
import router from './router'

// vue-multiselect
import Multiselect from 'vue-multiselect'
import 'vue-multiselect/dist/vue-multiselect.min.css'

// mdi
import '@mdi/font/css/materialdesignicons.css'

// iframe-resizer
import '@/directives/resize'

import buefyConfig from '@/buefy-config'
buefyConfig(Vue)

Vue.config.productionTip = false

Vue.component('multiselect', Multiselect)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
