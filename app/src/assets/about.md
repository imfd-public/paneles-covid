## Acerca de este sitio
Este sitio es el resultado de una colaboración entre el [**Instituto Milenio Fundamentos de los Datos**](https://imfd.cl) y el equipo de Dirección Médica del [**Área prestadores de Banmedica**](https://www.empresasbanmedica.cl/prestadores-de-salud/). El código se encuentra disponible en [este repositorio](https://gitlab.com/imfd-public/paneles-covid), ahí se pueden ver detalles técnicos y discusiones sobre los paneles.

## Datos
Para crear este sitio transformamos las planillas disponibles en el [repositorio de la Universidad Johns Hopkins](https://github.com/CSSEGISandData/COVID-19/) y en el [repositorio del Ministerio de Ciencia](https://github.com/MinCiencia/Datos-COVID19/) a una base de datos relacional. El proceso para crear dicha base de datos es completamente abierto, puedes ver todos los detalles y generar la base de datos tú mismo usando [este repositorio](https://gitlab.com/imfd-public/aggregated-covid-data).

**Importante:** Se ha dicho muchas veces que la calidad de los datos disponibles no es buena, al respecto recomendamos leer [esto](https://medium.com/@rbaeza_yates/datos-de-calidad-y-el-corona-virus-98893b7600e3) y [esto](https://medium.com/@marcelo.arenas/datos-abiertos-para-el-combate-del-coronavirus-1d634db20b3f) y no hacer proyecciones en base a estos datos.

**Recuperados en Chile:** Actualmente **no** estamos usando los datos de pacientes recuperados en Chile debido a las distintas polémicas que han habido al respecto (ver por ejemplo [este artículo](https://ciperchile.cl/2020/04/30/academico-acusa-que-estadistica-del-minsal-considera-recuperados-a-personas-que-estan-en-riesgo-de-morir/)).

## Buscamos colaboradores
Nos gustaría agregar más información, como las comunas en cuarentena u otros productos disponibles en el  [repositorio del Ministerio de Ciencia](https://github.com/MinCiencia/Datos-COVID19/). Esto significa bastante trabajo, te invitamos a colaborar en los repositorios mencionados más arriba.
