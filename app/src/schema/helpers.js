import moment from 'moment'
import categories from './categories.json'

export function retrieveRelevantForm(schema, form) {
  let result = { ...form }
  let properties =
    typeof schema.allOf === 'undefined'
      ? schema.properties
      : schema.allOf.reduce((obj, item) => Object.assign(obj, item.properties), {})

  for (const [k, v] of Object.entries(properties)) {
    if (v.exists === false || k.startsWith('_')) {
      delete result[k]
    } else if (result[k] instanceof Date) {
      result[k] = moment(result[k]).format('YYYY-MM-DD')
    } else if (v.$translateCategory === true) {
      result[k] = translateCategory(result[k])
    }
  }

  return result
}

export function retrieveUrl(schema, form) {
  let urlSchemaRules = schema.$url

  for (const rule of urlSchemaRules) {
    const fields = Object.keys(rule).filter(r => r.startsWith('_'))
    const matchesAndExistsInSchema = field => {
      return form[field] && form[field] === rule[field] && schema.exists !== false
    }

    if (fields.every(matchesAndExistsInSchema)) {
      return rule.url
    }
  }

  // Maybe raise exception here?
  return ''
}

export function translateCategory(category) {
  let translation = categories.find(cat => cat.translation === category)

  return translation.metabase
}
