import rawCategories from './categories.json'
import rawCountries from './countries.json'
import rawRegiones from './regiones.json'

const countries = rawCountries.sort()
const regions = rawRegiones.map(region => region.nombre_corto || region.nombre)
const categories = rawCategories.map(cat => cat.translation)

export default {
  $schema: 'http://json-schema.org/draft-07/schema#',
  title: 'Paneles Covid',
  description: 'Resumen datos relevantes para covid-19',
  type: 'object',
  properties: {
    perfil_chile: {
      title: 'Situación en Chile',
      allOf: [
        {
          type: 'object',
          title: 'Situación Chile-1',
          properties: {
            _a_partir_de: {
              type: 'string',
              const: 'Mostrar gráficos temporales desde el día',
              index: 1
            },
            date: {
              exists: true,
              type: 'string',
              title: 'Fecha de Inicio',
              format: 'date',
              default: new Date('2020-03-01'),
              index: 2
            }
          }
        },
        {
          type: 'object',
          title: 'Situación Chile-2',
          properties: {
            _test: {
              type: 'string',
              const: 'Mostrar cifras comparativas en',
              index: 3
            },
            _tipo_datos: {
              type: 'string',
              enum: ['tasas', 'totales'],
              default: 'tasas',
              index: 4
            }
          }
        }
      ],
      $url: [
        {
          _tipo_datos: 'totales',
          url: '/public/dashboard/1288bc85-d2cb-41f4-8abc-1dcd56ae8263'
        },
        {
          _tipo_datos: 'tasas',
          url: '/public/dashboard/ac48de05-4146-4d88-81e4-465cf0717bc6'
        }
      ]
    },
    situacion_mundial: {
      title: 'Situación Mundial',
      allOf: [
        {
          type: 'object',
          title: 'Situación mundial-1',
          properties: {
            _a_partir_de: {
              type: 'string',
              const: 'Mostrar gráficos temporales desde el día',
              index: 1
            },
            date: {
              exists: true,
              type: 'string',
              title: 'Fecha de Inicio',
              format: 'date',
              default: new Date('2020-03-01'),
              index: 2
            }
          }
        },
        {
          type: 'object',
          title: 'Situación mundial-2',
          properties: {
            _test: {
              type: 'string',
              const: 'Mostrar cifras comparativas en',
              index: 3
            },
            _tipo_datos: {
              type: 'string',
              enum: ['tasas', 'totales'],
              default: 'tasas',
              index: 4
            }
          }
        }
      ],
      $url: [
        {
          _tipo_datos: 'totales',
          url: '/public/dashboard/7ae75c7d-49cd-4383-b73c-613f275f122e'
        },
        {
          _tipo_datos: 'tasas',
          url: '/public/dashboard/6f478cca-ed02-485f-a902-c50b14458261'
        }
      ]
    },
    comparativo_paises: {
      title: 'Comparar países',
      allOf: [
        {
          type: 'object',
          title: 'Comparativo paises-1',
          properties: {
            _tipo_grafico_helper: {
              type: 'string',
              const: 'Comparar',
              index: 1
            },
            _tipo_grafico: {
              type: 'string',
              enum: ['tasas cada 100.000 habitantes', 'totales'],
              default: 'tasas cada 100.000 habitantes',
              index: 2
            },
            _de: {
              type: 'string',
              const: 'de',
              index: 3
            },
            countries: {
              type: 'array',
              title: 'Países',
              examples: countries,
              default: [
                'Chile',
                'Perú',
                'Alemania',
                'Brasil',
                'Estados Unidos',
                'Italia',
                'España'
              ],
              index: 4
            }
          }
        },
        {
          type: 'object',
          title: 'Comparativo paises-2',
          properties: {
            _test: {
              type: 'string',
              const: 'mostrando los gráficos temporales',
              index: 4.5
            },
            _segun: {
              type: 'string',
              enum: ['desde los', 'a partir del día'],
              examples: ['Número de pacientes', 'Una fecha'],
              default: 'desde los',
              index: 5,
              $actions: [
                {
                  trigger: {
                    value: 'a partir del día'
                  },
                  update: {
                    properties: {
                      n_hab: { exists: false },
                      category: { exists: false },
                      _hito: { exists: false },
                      date: {
                        exists: true,
                        type: 'string',
                        title: 'Fecha de Inicio',
                        format: 'date',
                        index: 6
                      }
                    }
                  }
                },
                {
                  trigger: {
                    value: 'desde los'
                  },
                  update: {
                    properties: {
                      date: { exists: false },
                      n_hab: {
                        exists: true,
                        type: 'number',
                        title: 'Cantidad de pacientes',
                        default: 50,
                        index: 7
                      },
                      category: {
                        exists: true,
                        type: 'string',
                        enum: categories.filter(cat =>
                          ['Confirmados', 'Fallecidos', 'Recuperados'].includes(cat)
                        ),
                        default: 'Confirmados',
                        index: 8,
                        $translateCategory: true
                      },
                      _hito: {
                        exists: true,
                        type: 'string',
                        enum: ['totales', 'por 100.000 habitantes'],
                        default: 'totales',
                        index: 9
                      }
                    }
                  }
                }
              ]
            }
          }
        }
      ],
      $url: [
        {
          _segun: 'a partir del día',
          _tipo_grafico: 'totales',
          url: '/public/dashboard/17b70a0c-9805-4e71-a724-68e18f2000d2'
        },
        {
          _segun: 'a partir del día',
          _tipo_grafico: 'tasas cada 100.000 habitantes',
          url: '/public/dashboard/8f0ab1d0-42b3-47db-a15b-f1796af170aa'
        },
        {
          _segun: 'desde los',
          _hito: 'totales',
          _tipo_grafico: 'totales',
          url: '/public/dashboard/6df232fc-f6f6-45bc-ad48-37f208bbd402'
        },
        {
          _segun: 'desde los',
          _hito: 'por 100.000 habitantes',
          _tipo_grafico: 'tasas cada 100.000 habitantes',
          url: '/public/dashboard/4a64e49e-2c42-4ef1-a839-5cf0139e538a'
        },
        {
          _segun: 'desde los',
          _hito: 'totales',
          _tipo_grafico: 'tasas cada 100.000 habitantes',
          url: '/public/dashboard/fe018705-4d1a-4afa-b9d7-fb3e7bb83fdf'
        },
        {
          _segun: 'desde los',
          _hito: 'por 100.000 habitantes',
          _tipo_grafico: 'totales',
          url: '/public/dashboard/bf6ad9c4-e5a1-4fbb-b02e-2dbf1c1ed8d6'
        }
      ]
    },
    comparativo_regiones: {
      title: 'Comparar regiones',
      allOf: [
        {
          type: 'object',
          title: 'Comparativo por regiones-1',
          properties: {
            _tipo_grafico_helper: {
              type: 'string',
              const: 'Comparar',
              index: 1
            },
            _tipo_grafico: {
              type: 'string',
              enum: ['tasas cada 100.000 habitantes', 'totales'],
              default: 'tasas cada 100.000 habitantes',
              index: 2
            },
            _de: {
              type: 'string',
              const: 'de',
              index: 3
            },
            regions: {
              type: 'array',
              title: 'Regiones',
              examples: regions,
              default: ['Metropolitana', 'Valparaíso', 'Magallanes'],
              index: 4
            }
          }
        },
        {
          type: 'object',
          title: 'Comparativo por regiones-2',
          properties: {
            _test: {
              type: 'string',
              const: 'mostrando los gráficos temporales',
              index: 4.5
            },
            _segun: {
              type: 'string',
              enum: ['desde los', 'a partir del día'],
              examples: ['Número de pacientes', 'Una fecha'],
              default: 'desde los',
              index: 5,
              $actions: [
                {
                  trigger: {
                    value: 'a partir del día'
                  },
                  update: {
                    properties: {
                      n_hab: { exists: false },
                      category: { exists: false },
                      _hito: { exists: false },
                      date: {
                        exists: true,
                        type: 'string',
                        title: 'Fecha de Inicio',
                        format: 'date',
                        index: 6
                      }
                    }
                  }
                },
                {
                  trigger: {
                    value: 'desde los'
                  },
                  update: {
                    properties: {
                      date: { exists: false },
                      n_hab: {
                        exists: true,
                        type: 'number',
                        title: 'Cantidad de pacientes',
                        default: 50,
                        index: 7
                      },
                      category: {
                        exists: true,
                        type: 'string',
                        enum: categories.filter(cat => ['Confirmados', 'Fallecidos'].includes(cat)),
                        default: 'Confirmados',
                        index: 8,
                        $translateCategory: true
                      },
                      _hito: {
                        exists: true,
                        type: 'string',
                        enum: ['totales', 'por 100.000 habitantes'],
                        default: 'totales',
                        index: 9
                      }
                    }
                  }
                }
              ]
            }
          }
        }
      ],
      $url: [
        {
          _segun: 'a partir del día',
          _tipo_grafico: 'totales',
          url: '/public/dashboard/91aed47a-8085-4d00-a88f-5f058f51c769'
        },
        {
          _segun: 'a partir del día',
          _tipo_grafico: 'tasas cada 100.000 habitantes',
          url: '/public/dashboard/4372cd0d-7625-49a9-a3d7-12e1dc60631c'
        },
        {
          _segun: 'desde los',
          _hito: 'totales',
          _tipo_grafico: 'totales',
          url: '/public/dashboard/d29114ea-5857-48e9-bce7-8d5618d059bb'
        },
        {
          _segun: 'desde los',
          _hito: 'por 100.000 habitantes',
          _tipo_grafico: 'tasas cada 100.000 habitantes',
          url: '/public/dashboard/230655b1-5489-4d80-9285-e690bc79f8db'
        },
        {
          _segun: 'desde los',
          _hito: 'totales',
          _tipo_grafico: 'tasas cada 100.000 habitantes',
          url: '/public/dashboard/688ae4ee-e0cd-4874-bab0-25656150dc22'
        },
        {
          _segun: 'desde los',
          _hito: 'por 100.000 habitantes',
          _tipo_grafico: 'totales',
          url: '/public/dashboard/f5fe377f-6f5f-441d-a7ca-ba20a1a01f7e'
        }
      ]
    },
    perfil_pais: {
      title: 'Situación en un país',
      type: 'object',
      properties: {
        _ver: {
          type: 'string',
          const: 'Ver situación de',
          title: 'En el país',
          index: 0
        },
        nombre_pa_s: {
          type: 'string',
          title: 'País',
          enum: countries,
          default: 'Chile',
          index: 1
        },
        _a_partir_de: {
          type: 'string',
          const: 'mostrando gráficos temporales desde el día',
          index: 1.5
        },
        fecha_de_inicio: {
          exists: true,
          type: 'string',
          title: 'Fecha de Inicio',
          format: 'date',
          default: new Date('2020-03-01'),
          index: 2
        }
      },
      $url: [
        {
          url: '/public/dashboard/61f6f911-496f-4469-989e-721370a26315'
        }
      ]
    },
    perfil_regional: {
      title: 'Situación en una región',
      type: 'object',
      properties: {
        _ver: {
          type: 'string',
          const: 'Región:',
          title: 'En la región',
          index: 0
        },
        region: {
          type: 'string',
          title: 'Región',
          enum: regions,
          default: 'Metropolitana',
          index: 1
        },
        _graficos_temporales: {
          type: 'string',
          const: 'Mostrar gráficos temporales a partir de',
          index: 1.5
        },
        fecha_de_inicio: {
          exists: true,
          type: 'string',
          title: 'Fecha de Inicio',
          format: 'date',
          default: new Date('2020-03-01'),
          index: 2
        }
      },
      $url: [
        {
          url: '/public/dashboard/40d8708f-ff2f-4e1e-881b-b02fe233a1f0'
        }
      ]
    }
  }
}
