// Buefy
import {
  Field,
  Modal,
  Toast,
  Datepicker,
  Numberinput,
  Icon,
  Button,
  Select,
  Navbar,
  Taginput
} from 'buefy'

export default function buefyConfig(vueInstance) {
  vueInstance.use(Navbar)
  vueInstance.use(Taginput)
  vueInstance.use(Button)
  vueInstance.use(Select)
  vueInstance.use(Datepicker)
  vueInstance.use(Numberinput)
  vueInstance.use(Icon)
  vueInstance.use(Field)
  vueInstance.use(Modal)
  vueInstance.use(Toast)
}
