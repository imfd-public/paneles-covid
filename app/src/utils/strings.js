export const normalize = word =>
  word
    .toLowerCase()
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')

export function fuzzyContains(target, query) {
  if (!target || !query) {
    return false // return early if target or query are undefined
  }

  if (target.length < query.length) {
    return false // impossible for query to be contained in target
  }

  const queryLower = normalize(query)
  const queryLen = queryLower.length
  const targetLower = normalize(target)

  let index = 0
  let lastIndexOf = -1
  while (index < queryLen) {
    const indexOf = targetLower.indexOf(queryLower[index], lastIndexOf + 1)
    if (indexOf < 0) {
      return false
    }

    lastIndexOf = indexOf

    index++
  }

  return true
}

export function capitalize(value) {
  if (!value || value.length === 0) {
    return value
  }

  return value.charAt(0).toUpperCase() + value.slice(1)
}
