import debounce from 'debounce-async'

const runDebounced = async debouncedFn => {
  // This try is because debounce-async rejects promises when a new event is
  // received. Assholes!
  try {
    return await debouncedFn()
  } catch (err) {
    if (err instanceof Error) {
      throw err
    }
  }
}

export { debounce, runDebounced }
