const classMap = {
  h1: 'title is-size-1',
  h2: 'title is-size-2',
  h3: 'title is-size-3',
  h4: 'title is-size-4',
  h5: 'title is-size-5',
  h6: 'title is-size-6',
  h7: 'title is-size-7',
  p: 'mb-1',
  ul: 'mb-1'
}

const bulmaShowdown = Object.keys(classMap).map(key => ({
  type: 'output',
  regex: new RegExp(`<${key}(.*)>`, 'g'),
  replace: `<${key} class="${classMap[key]}" $1>`
}))

export default bulmaShowdown
