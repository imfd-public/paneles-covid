import Vue from 'vue'
/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
function isObject(item) {
  return item && typeof item === 'object' && !Array.isArray(item)
}

/**
 * Deep merge two objects.
 * @param target
 * @param ...sources
 */
export function mergeDeep(target, ...sources) {
  if (!sources.length) return target
  const source = sources.shift()

  if (isObject(target) && isObject(source)) {
    for (const key in source) {
      if (isObject(source[key])) {
        if (!target[key]) {
          Vue.set(target, key, {})
        }
        mergeDeep(target[key], source[key])
      } else {
        Vue.set(target, key, source[key])
        // Object.assign(target, { [key]: source[key] })
      }
    }
  }

  return mergeDeep(target, ...sources)
}

export function deepFilter(target, cb) {
  if (!isObject(target)) {
    return target
  }

  let result = {}
  for (const key in target) {
    if (cb(key, target[key])) {
      result[key] = deepFilter(target[key], cb)
    }
  }

  return result
}
