function metabaseAbsoluteUrl(relativeUrl) {
  const base = process.env.VUE_APP_METABASE_BASE_URL || 'https://metabase.imfd.cl'
  const filler = relativeUrl.startsWith('/') ? '' : '/'

  return `${base}${filler}${relativeUrl}`
}

export { metabaseAbsoluteUrl }
