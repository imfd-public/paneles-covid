export default {
  props: {
    schema: {
      type: Object,
      required: true
    },
    value: {
      required: false
    }
  },
  data() {
    return {
      val: this.value
    }
  },
  methods: {
    emitValue() {
      if (this.val) {
        this.$emit('input', this.val)
      }
    }
  },
  watch: {
    value(newValue) {
      this.val = newValue
    },
    val() {
      this.emitValue()
    }
  }
}
