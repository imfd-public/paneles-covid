export default {
  props: {
    error: {
      type: [String, Array],
      required: false,
      default: ''
    }
  },
  computed: {
    errorMessage() {
      return this.error || null
    },
    isDangerIfHasErrors() {
      return {
        'is-danger': this.errorMessage
      }
    }
  }
}
